/// Parses the query params and return value of given key
function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

/// Return todays date tin YYYY-MM-DD format
function GetToday() {
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    return now.getFullYear() + "-" + (month) + "-" + (day);

}

$(document).ready(function () {

    // Set the default values for input boxes
    $("input#ip-date").val(GetURLParameter('date') || GetToday())
    var search = GetURLParameter('search') || "";
    $("input#ip-search").val(decodeURIComponent(search))

    
    $("#fetch-data").click(function () {
        var url = new URL(window.location.href);
        url.searchParams.set('date', $("input#ip-date").val());
        url.searchParams.set('search', $("input#ip-search").val());
        window.location.href = url.href;
    });

    $("a#previous").click(function () {
        var page = GetURLParameter('page') || 2;
        page = parseInt(page);
        page -= 1;

        var url = new URL(window.location.href);
        url.searchParams.set('page', page);
        window.location.href = url.href;

    });

    $("a#next").click(function () {
        var page = GetURLParameter('page') || 2;
        page = parseInt(page);
        page += 1;

        var url = new URL(window.location.href);
        url.searchParams.set('page', page);
        window.location.href = url.href;

    });
});



