"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import render_template
from SharesPlatform import app
from SharesPlatform.utils import scrap as scrip_utils;
from SharesPlatform.utils import store as store_utils;
from flask import request
import re

@app.route('/')
@app.route('/bse')
def bse():
    
    # Checking and adding default values for query parameters
    page = request.args.get("page");
    if page is None or not page.isdigit():
        page = 0

    
    limit = request.args.get("limit");
    if limit is None or not limit.isdigit():
        limit = 50
    
    search_term = request.args.get("search");

    date = request.args.get("date");
    if date is None:
        print("No date provided", date)
        date = datetime.now().strftime("%d%m%y")
    else:
        try:
            date = datetime.strptime(date, '%Y-%m-%d').strftime("%d%m%y")
        except:
            print("Wrong format of date ", date)
            date = datetime.now().strftime("%d%m%y")


    page = int(page)
    limit = int(limit)


    # Fetch data from redis server o bse website
    data = store_utils.get_data(date, type="EQ");
    total_pages = 0;
    total_records = 0
    total_records_in = 0
    if data:

        data.sort(key=lambda x:x["SC_CODE"])
        # If search term is provided, then filter the result
        if search_term:
            new_data = [];
            for s in data:
                if re.search(search_term, s["SC_NAME"], re.IGNORECASE):
                    new_data.append(s)
            data = new_data;
        
        data_len = total_records = len(data);
        total_pages = data_len//limit;
        page = min(page, total_pages)
        start_index = page*limit;
        end_index = start_index + limit;
        data = data[start_index:end_index];
        total_records_in = len(data)

    return render_template(
        'bse.jade',
        title='BSE index',
		data=data,
        total_pages=total_pages,
        current_page=page,
        total_records = total_records,
        total_records_in = total_records_in,
        year=datetime.now().year,
    )
