from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
import requests
import zipfile
import io
import csv
import json
import datetime

def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the
    text content, otherwise return None.
    """
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return None

def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200 and content_type is not None and content_type.find('html') > -1)

def log_error(e):
    """
    It is always a good idea to log errors. 
    This function just prints them, but you can
    make it do anything.
    """
    print(e)

def read_csv(file_name):
    """
    Reads the csv file and creates a object with following structure
        {
            SC_CODE: {
                SC_CODE: 1234,
                SC_NAME: "ABCD",
                ....
            },
            {....}
        }
    """

    data = {}
    with open(file_name) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        columns = []
        for row in csv_reader:
            if line_count == 0:
                columns = [x.strip() for x in row]
                line_count += 1
            else:
                x = {
                    'SC_CODE': row[0],
                    'SC_NAME': row[1],
                    'SC_GROUP': row[2],
                    'SC_TYPE': row[3],
                    'OPEN': row[4],
                    'HIGH': row[5],
                    'LOW': row[6],
                    'CLOSE': row[7],
                    'LAST': row[8],
                    'PREVCLOSE': row[9],
                    'NO_TRADES': row[10],
                    'NO_OF_SHRS': row[11],
                    'NET_TURNOV': row[12],
                    'TDCLOINDI': row[13],
                    }
                data[x['SC_CODE']] = json.dumps(x)

    return data
	
def scrap_page():
    """
    Parsed the page and tries to get EQ data for current date
    """
    html = simple_get('https://www.bseindia.com/markets/MarketInfo/BhavCopy.aspx')
    if html:
        formatted_html = BeautifulSoup(html, 'html.parser')
        zip_url = formatted_html.find("li", {"id": "ContentPlaceHolder1_liZip"}).a.get('href')
        r = requests.get(zip_url)
        if r.ok:
            z = zipfile.ZipFile(io.BytesIO(r.content))
            z.extractall('tmp')
            date = datetime.datetime.now().strftime("%d%m%y")
            return read_csv(date.strftime(f"tmp/EQ{date}.csv"))
    return None

def get_bhav_copy(date, zip_url=None):
    """
    Return the equity data from bse for given data
    Return None is data is not present in bse website
    """
    if zip_url is None:
        zip_url = f"https://www.bseindia.com/download/BhavCopy/Equity/EQ{date}_CSV.ZIP";

    print('download from bse')
    r = requests.get(zip_url)
    if r.ok:
        z = zipfile.ZipFile(io.BytesIO(r.content))
        z.extractall('tmp')
        return read_csv(f"tmp/EQ{date}.csv".upper())
    return None
    