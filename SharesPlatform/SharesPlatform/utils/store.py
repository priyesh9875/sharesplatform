import redis
import json
from SharesPlatform.utils import scrap as scrip_utils;
from SharesPlatform import config;


def get_data(date, type="EQ"):
    """
    Fetches the data from redis database, if not available then
    it tries to fetch from bse website and save it in redis database
    """

    # Check if data is present in redis or not
    # print ('Checking redis', date)
    data = get_data_redis(f'{type}{date}');
    if data:
        print ('Data in redis')
        return data
    print('Data not in redis')

    print('Checking bse')
    # Data not present in redis, try to fetch from bse website
    data = scrip_utils.get_bhav_copy(date);
    if data:
        print('data in bhav')

        # Save in redis and return the data
        save_data_redis(f'{type}{date}', data);
        return json_unload(data);

    # Data not available in redis and BSE website
    return None;

def get_data_redis(key):
    """
    Fetches data(dictionary/json) from redis server for given key value
    """
    conn = redis.Redis(host=config.redis_host, port=config.redis_port, password=config.redis_password)
    data = conn.hgetall(key)
    return json_unload(data);

def save_data_redis(key, data):
    """
    Saves the data(dictionary/json) in redis server for given key
    """
    conn = redis.Redis(host=config.redis_host, port=config.redis_port, password=config.redis_password)
    conn.hmset(key, data)

def json_unload(data):
    """
    Converts the data from redis to json
    """
    ret_val = []
    for k in data.keys():
        ret_val.append(json.loads(data[k]));

    return ret_val;
