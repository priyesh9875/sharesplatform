# SharesPlatform
Live demo [here](http://redis-zerodha.southindia.cloudapp.azure.com).

URL params:

- Date in YYYY-MM-DD format. [Example](http://redis-zerodha.southindia.cloudapp.azure.com/?date=2019-03-07)
- Search key: This accepts regex expression also. [Example](http://redis-zerodha.southindia.cloudapp.azure.com/?search=ip)
- Page: number which indicates which page to show. By default it shows 50 rows per page.
- Limit: number of items to show per page

### Example

URL: http://redis-zerodha.southindia.cloudapp.azure.com/?date=2019-03-07&search=%5E%5Cd%5Cd&page=0&limit=100

Retrieves copies of EQ for date 07 March 2019, and shows first page, with 100 rows


### How to run
Python > 3.6

- Install packages `pip3 install -m requirements.txt`
- Update `SharesPlatform/cofig.py` with redis database information
- Update 'host' and 'port' information in `runserver.py` file
- Run `python runserver.py`